---
layout: home

hero:
  name: Intranet-Docs
  text: Plataforma web 
  tagline: basada en wordpress
  image:
    src: /logo.png
    alt: wordpress
  actions:
    - theme: brand
      text: Comenzar
      link: /guide/intro
    - theme: brand
      text: Sys/Doc
      link: http://docs.fogade.gob.ve/
    - theme: alt
      text: GitLab
      link: https://gitlab.com/protiuna/doc-intranet-web
      

features:
  - title: Qué es  la intranet
    details: Es una plataforma digital cuyo objetivo es asistir a los trabajadores en la generación de valor para la institución , poniendo a su disposición activos como contenidos, archivos, procesos de negocio y herramientas; facilitando la colaboración y comunicación entre las personas y los equipos.
  - title: funcionamiento
    details: La intranet funciona gracias a los servidores en los que se aloja -propios o en la nube-, los usuarios generados y los dispositivos necesarios para conectarse a la misma. Una de las grandes ventajas es que los usuarios pueden acceder a ella desde cualquier lugar, y que se encuentra disponible las 24
 
---

